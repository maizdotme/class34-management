/*
Navicat MySQL Data Transfer

Source Server         : 云
Source Server Version : 50648
Source Host           : 49.234.126.83:3306
Source Database       : jufeng

Target Server Type    : MYSQL
Target Server Version : 50648
File Encoding         : 65001

Date: 2020-07-25 17:25:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for company_guest
-- ----------------------------
DROP TABLE IF EXISTS `company_guest`;
CREATE TABLE `company_guest` (
  `cgid` int(11) NOT NULL AUTO_INCREMENT,
  `cgname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `cgby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `cgby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `cgby3` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`cgid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept` (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `dname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `dby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for dept_permission
-- ----------------------------
DROP TABLE IF EXISTS `dept_permission`;
CREATE TABLE `dept_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `did` int(11) DEFAULT NULL,
  `mid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `ename` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `pwd` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `did` int(11) DEFAULT NULL,
  `eby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `eby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `eby3` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`eid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for employee_permission
-- ----------------------------
DROP TABLE IF EXISTS `employee_permission`;
CREATE TABLE `employee_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) DEFAULT NULL,
  `mid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for fmc
-- ----------------------------
DROP TABLE IF EXISTS `fmc`;
CREATE TABLE `fmc` (
  `fmcid` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `skzh` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `ywlx` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `fkf` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `dzqk` int(255) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `skqd` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `ysk` double(255,0) DEFAULT NULL,
  `ssk` double(255,2) DEFAULT NULL,
  `hxr` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`fmcid`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for guest
-- ----------------------------
DROP TABLE IF EXISTS `guest`;
CREATE TABLE `guest` (
  `gid` int(11) NOT NULL AUTO_INCREMENT,
  `gname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `sex` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `birth` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `cred_id` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `gby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `gby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `gby3` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for guest_info
-- ----------------------------
DROP TABLE IF EXISTS `guest_info`;
CREATE TABLE `guest_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` int(11) DEFAULT NULL,
  `oid` int(11) DEFAULT NULL,
  `eid` int(11) DEFAULT NULL,
  `time` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `outcode` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `gclass` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `cgid` int(11) DEFAULT NULL,
  `giby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `giby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `giby3` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for guide
-- ----------------------------
DROP TABLE IF EXISTS `guide`;
CREATE TABLE `guide` (
  `gid` int(11) NOT NULL AUTO_INCREMENT,
  `gname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `degree` int(11) DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `status` int(255) DEFAULT NULL,
  `gby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `gby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `gby3` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for helf_cost
-- ----------------------------
DROP TABLE IF EXISTS `helf_cost`;
CREATE TABLE `helf_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `hename` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `hcby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `hcby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for hotel
-- ----------------------------
DROP TABLE IF EXISTS `hotel`;
CREATE TABLE `hotel` (
  `hid` int(12) NOT NULL AUTO_INCREMENT COMMENT '酒店id',
  `hname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `star` int(10) DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `hcby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `hcby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`hid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for hotel_cost
-- ----------------------------
DROP TABLE IF EXISTS `hotel_cost`;
CREATE TABLE `hotel_cost` (
  `hcid` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `hid` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `hcby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `hcby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`hcid`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for insurance_cost
-- ----------------------------
DROP TABLE IF EXISTS `insurance_cost`;
CREATE TABLE `insurance_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `icid` int(11) DEFAULT NULL,
  `total` double(11,2) DEFAULT NULL,
  `icby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `icby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for insurancecompany
-- ----------------------------
DROP TABLE IF EXISTS `insurancecompany`;
CREATE TABLE `insurancecompany` (
  `icid` int(11) NOT NULL AUTO_INCREMENT,
  `icname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `icby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `icby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`icid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `mname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `parent` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `level` int(255) DEFAULT NULL,
  `mby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `mby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `mby3` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for order_trip
-- ----------------------------
DROP TABLE IF EXISTS `order_trip`;
CREATE TABLE `order_trip` (
  `otid` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `daytime` int(11) DEFAULT NULL,
  `trip_detail` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `ptby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `ptby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`otid`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `oid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `oclass` int(255) DEFAULT NULL,
  `oname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `ocla1` int(255) DEFAULT NULL,
  `ocla2` int(255) DEFAULT NULL,
  `geid` int(11) DEFAULT NULL,
  `jeid` int(11) DEFAULT NULL,
  `isdel` int(255) DEFAULT NULL,
  `ordercode` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `total` double(255,0) DEFAULT NULL,
  `oby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `oby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `oby3` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for pay
-- ----------------------------
DROP TABLE IF EXISTS `pay`;
CREATE TABLE `pay` (
  `payid` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL COMMENT '1代表已付款，2代表未付款',
  `jtcb` double(255,2) DEFAULT NULL,
  `zscb` double(255,2) DEFAULT NULL,
  `cycb` double(255,2) DEFAULT NULL,
  `bxcb` double(255,2) DEFAULT NULL,
  `jqcb` double(255,2) DEFAULT NULL,
  `dycb` double(255,2) DEFAULT NULL,
  `yfk` double(255,2) DEFAULT NULL,
  `sfk` double(255,2) DEFAULT NULL,
  `hxr` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `byzd1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `byzd2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `byzd3` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `byzd4` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`payid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for proclass
-- ----------------------------
DROP TABLE IF EXISTS `proclass`;
CREATE TABLE `proclass` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `pname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `pnumber` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `price` double(10,0) DEFAULT NULL,
  `spots` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `pclass` int(11) DEFAULT NULL,
  `isdel` int(11) DEFAULT NULL,
  `pby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `pby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `pby3` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for product_trip
-- ----------------------------
DROP TABLE IF EXISTS `product_trip`;
CREATE TABLE `product_trip` (
  `ptid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `daytime` int(11) DEFAULT NULL,
  `trip_detail` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `ptby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `ptby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`ptid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for restaurant
-- ----------------------------
DROP TABLE IF EXISTS `restaurant`;
CREATE TABLE `restaurant` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `rname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `rcby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `rcby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for restaurant_cost
-- ----------------------------
DROP TABLE IF EXISTS `restaurant_cost`;
CREATE TABLE `restaurant_cost` (
  `rcid` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `rid` int(11) DEFAULT NULL,
  `price` double(200,2) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `total` double(255,2) DEFAULT NULL,
  `rcby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `rcby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`rcid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for spot
-- ----------------------------
DROP TABLE IF EXISTS `spot`;
CREATE TABLE `spot` (
  `spot_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '(景点id，主键，自增)',
  `spot_name` varchar(255) DEFAULT NULL COMMENT '(景点名)',
  `spot_price` decimal(10,2) unsigned DEFAULT NULL COMMENT '(景点价格) 门票',
  `spot_location` int(11) unsigned DEFAULT NULL COMMENT '(景点位置)所属城市/对应城市表ID',
  `spot_remarks` varchar(255) DEFAULT NULL COMMENT '(备注)',
  `spot_content` text COMMENT '(景点介绍)',
  `spot_longitude` decimal(10,6) DEFAULT NULL COMMENT '经度',
  `spot_latitude` decimal(10,6) DEFAULT NULL COMMENT '纬度',
  `spot_address` varchar(255) DEFAULT NULL COMMENT '景点地址',
  `uid` varchar(255) DEFAULT NULL,
  `spot_telephone` varchar(255) DEFAULT NULL COMMENT '景点电话',
  `spot_img` varchar(255) DEFAULT NULL COMMENT '景点缩略图',
  PRIMARY KEY (`spot_id`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5701 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for spot_cost
-- ----------------------------
DROP TABLE IF EXISTS `spot_cost`;
CREATE TABLE `spot_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `spot_id` int(11) DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `total` double(10,2) DEFAULT NULL,
  `scby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `scby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

-- ----------------------------
-- Table structure for transport_cost
-- ----------------------------
DROP TABLE IF EXISTS `transport_cost`;
CREATE TABLE `transport_cost` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) DEFAULT NULL,
  `cname` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `tcby1` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  `tcby2` varchar(255) COLLATE utf8_croatian_ci DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;
