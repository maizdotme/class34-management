/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.65-MariaDB : Database - yoga_shop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`yoga_shop` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `yoga_shop`;

/*Table structure for table `shop_address` */

DROP TABLE IF EXISTS `shop_address`;

CREATE TABLE `shop_address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `address_userName` varchar(10) DEFAULT NULL,
  `address_phone` varchar(15) DEFAULT NULL,
  `address_name` varchar(50) DEFAULT NULL,
  `address_static` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

/*Data for the table `shop_address` */

insert  into `shop_address`(`address_id`,`user_id`,`address_userName`,`address_phone`,`address_name`,`address_static`) values (11,51,'黄安','15683321123','重庆市渝北区金鹰','1'),(12,51,'老大','1109632636','重庆市南岸区','1'),(13,62,'huangan','123456','重庆市江北','1'),(14,70,'绿岸','18888888','花卉园股骨干 ','1'),(15,77,'黄黄','18323269003','渝北','1');

/*Table structure for table `shop_comment` */

DROP TABLE IF EXISTS `shop_comment`;

CREATE TABLE `shop_comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) DEFAULT NULL,
  `comment_context` varchar(50) DEFAULT NULL,
  `comment_time` varchar(20) DEFAULT NULL,
  `comment_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `shop_comment` */

/*Table structure for table `shop_goods` */

DROP TABLE IF EXISTS `shop_goods`;

CREATE TABLE `shop_goods` (
  `goods_id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_name` varchar(10) DEFAULT NULL,
  `goods_score` int(11) DEFAULT NULL,
  `goods_discount` int(11) DEFAULT NULL,
  `goods_image` varchar(50) DEFAULT NULL,
  `goods_stock` int(11) DEFAULT NULL,
  `member_level` int(11) DEFAULT NULL,
  `goods_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

/*Data for the table `shop_goods` */

insert  into `shop_goods`(`goods_id`,`goods_name`,`goods_score`,`goods_discount`,`goods_image`,`goods_stock`,`member_level`,`goods_status`) values (10,'瑜伽气垫',200,100,'http://qd786hos4.bkt.clouddn.com/5cdb8e44513245988',515,1,'1'),(11,'瑜伽衣服',300,100,'http://qd786hos4.bkt.clouddn.com/55eb30cafff3427eb',465,1,'1'),(12,'瑜伽衣服',300,100,'http://qd786hos4.bkt.clouddn.com/cfc47da883334c12a',420,1,'1'),(13,'瑜伽水杯',150,100,'http://qd786hos4.bkt.clouddn.com/86157100f54d4b7d9',457,1,'1'),(14,'瑜伽毛巾',150,100,'http://qd786hos4.bkt.clouddn.com/a5f39d5f90ab4298b',124,1,'1');

/*Table structure for table `shop_orders` */

DROP TABLE IF EXISTS `shop_orders`;

CREATE TABLE `shop_orders` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `orders_score` int(11) DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL,
  `goods_nums` int(11) DEFAULT NULL,
  `orders_user_name` varchar(50) DEFAULT NULL,
  `orders_user_phone` varchar(50) DEFAULT NULL,
  `orders_user_address` varchar(50) DEFAULT NULL,
  `orders_no` varchar(30) DEFAULT NULL,
  `orders_time` varchar(50) DEFAULT NULL,
  `orders_status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`orders_id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8mb4;

/*Data for the table `shop_orders` */

insert  into `shop_orders`(`orders_id`,`user_id`,`orders_score`,`goods_id`,`goods_nums`,`orders_user_name`,`orders_user_phone`,`orders_user_address`,`orders_no`,`orders_time`,`orders_status`) values (163,51,200,10,1,'黄安','15683321123','重庆市渝北区金鹰','WNCQ1595431531070','Wed Jul 22 23:25:31 CST 2020','2'),(164,51,600,10,3,'老大','1109632636','重庆市南岸区','WNCQ1595431645017','Wed Jul 22 23:27:25 CST 2020','3'),(165,51,1000,10,5,'黄安','15683321123','重庆市渝北区金鹰','WNCQ1595431701184','Wed Jul 22 23:28:21 CST 2020','2'),(166,51,1000,10,5,'黄安','15683321123','重庆市渝北区金鹰','WNCQ1595431713713','Wed Jul 22 23:28:33 CST 2020','2'),(167,51,200,10,1,'老大','1109632636','重庆市南岸区','WNCQ1595431835226','Wed Jul 22 23:30:35 CST 2020','3'),(168,62,1000,10,5,'huangan','123456','重庆市江北','WNCQ1595558760036','Fri Jul 24 10:46:00 CST 2020','3'),(169,70,800,10,4,'绿岸','18888888','花卉园股骨干 ','WNCQ1595575148824','Fri Jul 24 15:19:10 CST 2020','2'),(170,70,800,10,4,'绿岸','18888888','花卉园股骨干 ','WNCQ1595575150898','Fri Jul 24 15:19:11 CST 2020','3'),(171,77,1600,10,8,'黄黄','18323269003','渝北','WNCQ1595580623202','Fri Jul 24 16:50:23 CST 2020','2'),(172,77,1200,13,8,'黄黄','18323269003','渝北','WNCQ1595580746827','Fri Jul 24 16:52:27 CST 2020','2'),(173,77,900,12,3,'黄黄','18323269003','渝北','WNCQ1595580781929','Fri Jul 24 16:53:02 CST 2020','2'),(174,77,900,12,3,'黄黄','18323269003','渝北','WNCQ1595580786757','Fri Jul 24 16:53:06 CST 2020','2'),(175,77,900,12,3,'黄黄','18323269003','渝北','WNCQ1595580793324','Fri Jul 24 16:53:13 CST 2020','3'),(176,77,750,13,5,'黄黄','18323269003','渝北','WNCQ1595580905393','Fri Jul 24 16:55:05 CST 2020','2');

/*Table structure for table `shop_orderscan` */

DROP TABLE IF EXISTS `shop_orderscan`;

CREATE TABLE `shop_orderscan` (
  `orderscan_id` int(11) NOT NULL AUTO_INCREMENT,
  `orderscan_reason` varchar(50) DEFAULT NULL,
  `orders_id` int(11) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`orderscan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4;

/*Data for the table `shop_orderscan` */

insert  into `shop_orderscan`(`orderscan_id`,`orderscan_reason`,`orders_id`,`status`) values (36,'超过30分钟,未支付,自动取消订单',163,'1'),(37,'超过30分钟,未支付,自动取消订单',165,'1'),(38,'超过30分钟,未支付,自动取消订单',166,'1'),(39,'超过30分钟,未支付,自动取消订单',168,'1'),(40,'超过30分钟,未支付,自动取消订单',169,'1'),(41,'超过30分钟,未支付,自动取消订单',171,'1'),(42,'超过30分钟,未支付,自动取消订单',172,'1'),(43,'超过30分钟,未支付,自动取消订单',173,'1'),(44,'超过30分钟,未支付,自动取消订单',174,'1'),(45,'超过30分钟,未支付,自动取消订单',176,'1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
