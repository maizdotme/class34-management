/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.65-MariaDB : Database - yoga
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`yoga` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `yoga`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `acccount_id` int(11) NOT NULL AUTO_INCREMENT,
  `acccount_name` varchar(255) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  `money` double(255,0) DEFAULT NULL,
  `status` int(255) DEFAULT NULL,
  PRIMARY KEY (`acccount_id`)
) ENGINE=InnoDB AUTO_INCREMENT=492 DEFAULT CHARSET=utf8;

/*Data for the table `account` */

insert  into `account`(`acccount_id`,`acccount_name`,`a`,`b`,`c`,`money`,`status`) values (488,'KUXM1DZ3',NULL,NULL,NULL,42550,1),(489,'NFX9XFW1',NULL,NULL,NULL,1,1),(490,'JKD03VZF',NULL,NULL,NULL,50,1),(491,'2FO98QPA',NULL,NULL,NULL,50,1);

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  `money` double DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`admin_id`,`admin_name`,`password`,`a`,`b`,`c`,`money`) values (1,'admin','admin',NULL,NULL,NULL,750);

/*Table structure for table `adress` */

DROP TABLE IF EXISTS `adress`;

CREATE TABLE `adress` (
  `adress_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `longtitude` double(255,6) DEFAULT NULL,
  `latitude` double(255,6) DEFAULT NULL,
  PRIMARY KEY (`adress_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

/*Data for the table `adress` */

insert  into `adress`(`adress_id`,`user_id`,`longtitude`,`latitude`) values (28,77,109.124785,29.122888),(29,78,109.124857,29.123456),(30,79,109.122629,29.122699),(31,80,0.000000,0.000000);

/*Table structure for table `advertising` */

DROP TABLE IF EXISTS `advertising`;

CREATE TABLE `advertising` (
  `advertising_id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`advertising_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

/*Data for the table `advertising` */

insert  into `advertising`(`advertising_id`,`img`,`description`,`start_time`,`end_time`,`a`,`b`,`c`) values (36,'http://qd786hos4.bkt.clouddn.com/9ef56b1c380f41aea3d6a9ba587b3ce6','guangao1','2000-11-14 00:00:00','2000-12-14 00:00:00',NULL,NULL,NULL);

/*Table structure for table `attention` */

DROP TABLE IF EXISTS `attention`;

CREATE TABLE `attention` (
  `attention_id` int(11) NOT NULL AUTO_INCREMENT,
  `oneself_id` int(11) DEFAULT NULL,
  `others_id` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`attention_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `attention` */

insert  into `attention`(`attention_id`,`oneself_id`,`others_id`,`category`) values (22,78,77,NULL),(23,77,78,NULL);

/*Table structure for table `charge_confirm` */

DROP TABLE IF EXISTS `charge_confirm`;

CREATE TABLE `charge_confirm` (
  `charge_confirm_id` int(11) NOT NULL AUTO_INCREMENT,
  `coach_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `status` int(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `coach_time_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`charge_confirm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

/*Data for the table `charge_confirm` */

insert  into `charge_confirm`(`charge_confirm_id`,`coach_id`,`student_id`,`status`,`time`,`coach_time_id`) values (70,78,77,1,'2020-07-24 16:37:14',NULL);

/*Table structure for table `coach_course` */

DROP TABLE IF EXISTS `coach_course`;

CREATE TABLE `coach_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coach_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

/*Data for the table `coach_course` */

insert  into `coach_course`(`id`,`coach_id`,`course_id`,`a`,`b`,`c`) values (104,78,5,NULL,NULL,NULL),(105,78,6,NULL,NULL,NULL);

/*Table structure for table `coach_info` */

DROP TABLE IF EXISTS `coach_info`;

CREATE TABLE `coach_info` (
  `coach_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `personal_trainer` int(11) DEFAULT NULL,
  `genre_id` int(11) DEFAULT NULL,
  `tel_level` int(11) DEFAULT NULL COMMENT '电话号码开放等级1.开放所有人2.开放给好友',
  `phone` varchar(255) DEFAULT NULL,
  `charge` int(11) DEFAULT NULL COMMENT '收费标准(整数)',
  `indentification` int(11) DEFAULT NULL COMMENT '教练认证 1.未认证2.场馆认证3.官方认证 ',
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`coach_info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

/*Data for the table `coach_info` */

insert  into `coach_info`(`coach_info_id`,`user_id`,`personal_trainer`,`genre_id`,`tel_level`,`phone`,`charge`,`indentification`,`a`,`b`,`c`) values (19,78,NULL,NULL,3,NULL,500,2,NULL,'3',NULL);

/*Table structure for table `coach_student` */

DROP TABLE IF EXISTS `coach_student`;

CREATE TABLE `coach_student` (
  `coach_student_id` int(11) NOT NULL AUTO_INCREMENT,
  `coach_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  `status` int(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`coach_student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `coach_student` */

insert  into `coach_student`(`coach_student_id`,`coach_id`,`student_id`,`a`,`b`,`c`,`status`,`time`) values (22,78,77,NULL,NULL,NULL,1,'2020-07-24 16:38:01');

/*Table structure for table `coach_time` */

DROP TABLE IF EXISTS `coach_time`;

CREATE TABLE `coach_time` (
  `coach_time_id` int(11) NOT NULL AUTO_INCREMENT,
  `time_id` int(11) DEFAULT NULL,
  `week` int(255) DEFAULT NULL COMMENT '星期一到星期五',
  `status` int(255) DEFAULT NULL,
  `coach_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`coach_time_id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

/*Data for the table `coach_time` */

insert  into `coach_time`(`coach_time_id`,`time_id`,`week`,`status`,`coach_id`) values (101,1,1,1,78),(102,2,1,1,78),(103,3,1,1,78),(104,1,2,1,78),(105,2,2,1,78),(106,3,2,1,78),(107,1,3,1,78),(108,2,3,1,78),(109,3,3,1,78),(110,1,4,1,78),(111,2,4,1,78),(112,3,4,1,78),(113,1,5,1,78),(114,2,5,1,78),(115,3,5,1,78);

/*Table structure for table `comment` */

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_content` varchar(255) DEFAULT NULL,
  `comment_time` datetime DEFAULT NULL,
  `orders_id` int(11) DEFAULT NULL,
  `level` int(255) DEFAULT NULL,
  `coach_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `comment` */

/*Table structure for table `course` */

DROP TABLE IF EXISTS `course`;

CREATE TABLE `course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `course` */

insert  into `course`(`course_id`,`course_name`,`a`,`b`,`c`) values (5,'Garuda asana （Garuda pose） ﹝神鸟式﹞',NULL,NULL,NULL),(6,' Utthita Trikona asana （Extended Triangle pose） 扩大三角式',NULL,NULL,NULL),(7,'Parivritta Trikona asana （Twisted Triangle pose） 扭转三角式',NULL,NULL,NULL),(8,'Ardha Chandra asana （Half Moon pose） 半月式',NULL,NULL,NULL),(9,'Tada asana （Mountain pose） 山式',NULL,NULL,NULL),(10,'Urdhva Hasta asana （Raised Arm pose） 举臂式',NULL,NULL,NULL),(11,' Vriksha asana （Tree pose） 树式',NULL,NULL,NULL),(12,' Utkata asana （High-mighty pose） 高力式﹝椅子式﹞',NULL,NULL,NULL);

/*Table structure for table `genre` */

DROP TABLE IF EXISTS `genre`;

CREATE TABLE `genre` (
  `genre_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '流派类名称',
  `genre_name` varchar(255) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='genre(瑜伽流派类型表)';

/*Data for the table `genre` */

/*Table structure for table `inform` */

DROP TABLE IF EXISTS `inform`;

CREATE TABLE `inform` (
  `inform_id` int(11) NOT NULL AUTO_INCREMENT,
  `write_id` int(11) DEFAULT NULL,
  `read_id` int(11) DEFAULT NULL,
  `message` text,
  `type` int(255) DEFAULT NULL COMMENT '消息类型id',
  `status` int(255) DEFAULT NULL COMMENT '1.未读 2.已读',
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`inform_id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8;

/*Data for the table `inform` */

insert  into `inform`(`inform_id`,`write_id`,`read_id`,`message`,`type`,`status`,`a`,`b`,`c`,`time`) values (73,77,78,'您有新的学员预约',1,2,NULL,NULL,NULL,'2020-07-24 16:36:54'),(74,78,77,'签约成功',1,2,NULL,NULL,NULL,'2020-07-24 16:38:01'),(75,78,77,'[温]关注了你',1,2,NULL,NULL,NULL,'2020-07-24 16:38:53'),(76,77,78,'[用户昵称一]关注了你',1,2,NULL,NULL,NULL,'2020-07-24 16:39:11'),(77,78,77,'付款成功',1,1,NULL,NULL,NULL,'2020-07-24 16:44:39'),(78,77,78,'收到一笔资金',1,1,NULL,NULL,NULL,'2020-07-24 16:44:39');

/*Table structure for table `invitation` */

DROP TABLE IF EXISTS `invitation`;

CREATE TABLE `invitation` (
  `invitation_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `invitation_time` datetime DEFAULT NULL,
  `invitation_privilege` int(255) DEFAULT NULL COMMENT '帖子的权限',
  `votes` int(255) DEFAULT NULL COMMENT '点赞数',
  `img_src` text,
  PRIMARY KEY (`invitation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `invitation` */

/*Table structure for table `member` */

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `level` int(255) DEFAULT NULL COMMENT '会员等级 与积分有关',
  `aggregate_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/*Data for the table `member` */

insert  into `member`(`member_id`,`user_id`,`score`,`level`,`aggregate_score`) values (39,77,'9100',5,'10000'),(40,78,'10000',1,'10000'),(41,79,'10000',1,'10000'),(42,80,'3000',1,'3000');

/*Table structure for table `message` */

DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_content` varchar(255) DEFAULT NULL,
  `write_id` int(11) DEFAULT NULL,
  `read_id` int(11) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  `message_time` datetime DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

/*Data for the table `message` */

insert  into `message`(`message_id`,`message_content`,`write_id`,`read_id`,`a`,`b`,`c`,`message_time`) values (84,'你好！',78,77,NULL,NULL,NULL,'2020-07-24 16:39:55'),(85,'时代峰峻',77,78,NULL,NULL,NULL,'2020-07-24 16:40:00'),(86,'胜多负少',78,77,NULL,NULL,NULL,'2020-07-24 16:40:11'),(87,'撒地方',78,77,NULL,NULL,NULL,'2020-07-24 16:40:13'),(88,'胜多负少的',78,77,NULL,NULL,NULL,'2020-07-24 16:40:17'),(89,'神鼎飞丹砂',77,78,NULL,NULL,NULL,'2020-07-24 16:40:21'),(90,'胜多负少的',77,78,NULL,NULL,NULL,'2020-07-24 16:40:22'),(91,'撒地方',77,78,NULL,NULL,NULL,'2020-07-24 16:40:23'),(92,'胜多负少的',78,77,NULL,NULL,NULL,'2020-07-24 16:40:27');

/*Table structure for table `notice` */

DROP TABLE IF EXISTS `notice`;

CREATE TABLE `notice` (
  `notice_id` int(255) NOT NULL AUTO_INCREMENT,
  `notice_time` date DEFAULT NULL,
  `notice_content` text,
  `status` int(255) DEFAULT NULL,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `notice` */

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `orders_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `course_id` int(11) DEFAULT NULL COMMENT '课程id',
  `coach_id` int(11) DEFAULT NULL COMMENT '教练id',
  `course_length` int(11) DEFAULT NULL COMMENT '课时总 长',
  `money` double(255,0) DEFAULT NULL COMMENT '全部花费',
  `status` int(11) DEFAULT NULL COMMENT '订单状态 状态分为:支付中 支付失败 ( 过期或者被拒) 完成交易',
  `coach_time_id` int(11) DEFAULT NULL COMMENT '教练时间表',
  `student_id` int(11) DEFAULT NULL COMMENT '学员id',
  PRIMARY KEY (`orders_id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

/*Data for the table `orders` */

insert  into `orders`(`orders_id`,`course_id`,`coach_id`,`course_length`,`money`,`status`,`coach_time_id`,`student_id`) values (82,5,78,15,500,3,101,77);

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `posts` int(255) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`posts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `posts` */

/*Table structure for table `record` */

DROP TABLE IF EXISTS `record`;

CREATE TABLE `record` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `record_time` datetime DEFAULT NULL,
  `course_content` varchar(255) NOT NULL,
  `stutent_name` varchar(255) DEFAULT NULL,
  `coach_name` varchar(255) DEFAULT NULL,
  `money` int(255) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `a` int(255) DEFAULT NULL,
  `orders_id` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `record` */

/*Table structure for table `time` */

DROP TABLE IF EXISTS `time`;

CREATE TABLE `time` (
  `time_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `time_description` varchar(255) DEFAULT NULL COMMENT '早 8点-12点  ,11点-18点为中,17点-22点为晚',
  PRIMARY KEY (`time_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `time` */

insert  into `time`(`time_id`,`name`,`time_description`) values (1,'早','上午8:00-中午12:00'),(2,'中','中午1:00-下午5:00'),(3,'晚','晚上7:00-晚上10:00');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(255) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT '',
  `username` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL COMMENT '精读',
  `latitude` varchar(255) DEFAULT NULL COMMENT '纬度',
  `a` int(255) DEFAULT NULL COMMENT '表示是否完善信息1.未完善2.已完善',
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`) USING BTREE,
  UNIQUE KEY `tel` (`tel`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`user_id`,`email`,`tel`,`password`,`username`,`role_id`,`status`,`longitude`,`latitude`,`a`,`b`,`c`) values (77,NULL,'18883725295','root1234','何智航',1,2,NULL,NULL,2,NULL,NULL),(78,NULL,'18323269003','root123','温力量',2,2,NULL,NULL,2,NULL,NULL),(79,NULL,'15683387445','root1234',NULL,3,2,NULL,NULL,2,NULL,NULL),(80,NULL,'18883296574','root1234',NULL,1,2,NULL,NULL,1,NULL,NULL);

/*Table structure for table `userinfo` */

DROP TABLE IF EXISTS `userinfo`;

CREATE TABLE `userinfo` (
  `userinfo_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `real_name` varchar(255) DEFAULT NULL,
  `head_portrait` varchar(255) DEFAULT NULL COMMENT '头像',
  `nickname` varchar(255) DEFAULT NULL,
  `info_level` int(255) DEFAULT NULL,
  `pre_login_address` varchar(255) DEFAULT NULL COMMENT '上次登录地址',
  `acccount_id` int(11) DEFAULT NULL,
  `qq` varchar(255) DEFAULT NULL,
  `wechat` varchar(255) DEFAULT NULL,
  `telphone` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userinfo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='上次登录地址';

/*Data for the table `userinfo` */

insert  into `userinfo`(`userinfo_id`,`user_id`,`real_name`,`head_portrait`,`nickname`,`info_level`,`pre_login_address`,`acccount_id`,`qq`,`wechat`,`telphone`,`a`,`b`,`c`) values (43,77,NULL,'http://qd786hos4.bkt.clouddn.com/bf3e368f-de0a-47d5-8f12-3ae56f3155f6.jpg','用户昵称一',3,NULL,488,NULL,NULL,NULL,NULL,NULL,NULL),(44,78,'文力量','http://qd786hos4.bkt.clouddn.com/7dd7a707-e1ba-402e-856b-1fbf047c1846.jpg','温',3,NULL,489,NULL,NULL,NULL,NULL,NULL,NULL),(45,79,'黄安','http://qd786hos4.bkt.clouddn.com/13f0ace4-9df6-41de-8b1f-1f5e2c2ce945.jpg','huangan',2,NULL,490,NULL,NULL,NULL,NULL,NULL,NULL),(46,80,NULL,'http://qd786hos4.bkt.clouddn.com/13f0ace4-9df6-41de-8b1f-1f5e2c2ce945.jpg','CGHJ17IV',2,NULL,491,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `yoga_gym_img` */

DROP TABLE IF EXISTS `yoga_gym_img`;

CREATE TABLE `yoga_gym_img` (
  `yoga_gym_img_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`yoga_gym_img_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;

/*Data for the table `yoga_gym_img` */

insert  into `yoga_gym_img`(`yoga_gym_img_id`,`user_id`,`img`,`a`,`b`,`c`) values (75,79,'http://qd786hos4.bkt.clouddn.com/b8ee3b0371104641a0eb6f90366be46d',NULL,NULL,NULL),(76,79,'http://qd786hos4.bkt.clouddn.com/4276e21c261e436e918e2da21926d6cd',NULL,NULL,NULL),(77,79,'http://qd786hos4.bkt.clouddn.com/56ae8a13765d4e82babcc9bfc9bbaf2e',NULL,NULL,NULL);

/*Table structure for table `yoga_gym_info` */

DROP TABLE IF EXISTS `yoga_gym_info`;

CREATE TABLE `yoga_gym_info` (
  `yoga_gym_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `yoga_gym_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `yoga_gym_details` text,
  `a1` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`yoga_gym_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `yoga_gym_info` */

insert  into `yoga_gym_info`(`yoga_gym_id`,`user_id`,`yoga_gym_name`,`phone`,`yoga_gym_details`,`a1`,`b`,`c`) values (33,79,'皇家场馆','15683387444','很大',2,NULL,NULL);

/*Table structure for table `yoga_gym_product` */

DROP TABLE IF EXISTS `yoga_gym_product`;

CREATE TABLE `yoga_gym_product` (
  `yoga_gym_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `sold` int(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`yoga_gym_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `yoga_gym_product` */

/*Table structure for table `yoga_gym_signed_coach` */

DROP TABLE IF EXISTS `yoga_gym_signed_coach`;

CREATE TABLE `yoga_gym_signed_coach` (
  `yoga_gym_signed_coach_id` int(11) NOT NULL AUTO_INCREMENT,
  `yoga_gym_id` int(11) DEFAULT NULL,
  `coach_id` int(11) DEFAULT NULL,
  `status` int(255) DEFAULT NULL,
  `a` int(255) DEFAULT NULL,
  `b` varchar(255) DEFAULT NULL,
  `c` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`yoga_gym_signed_coach_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

/*Data for the table `yoga_gym_signed_coach` */

insert  into `yoga_gym_signed_coach`(`yoga_gym_signed_coach_id`,`yoga_gym_id`,`coach_id`,`status`,`a`,`b`,`c`) values (55,79,78,2,NULL,NULL,NULL);

/*Table structure for table `v_demo` */

DROP TABLE IF EXISTS `v_demo`;

/*!50001 DROP VIEW IF EXISTS `v_demo` */;
/*!50001 DROP TABLE IF EXISTS `v_demo` */;

/*!50001 CREATE TABLE  `v_demo`(
 `user_id` int(255) ,
 `tel` varchar(255) 
)*/;

/*Table structure for table `v_demo1` */

DROP TABLE IF EXISTS `v_demo1`;

/*!50001 DROP VIEW IF EXISTS `v_demo1` */;
/*!50001 DROP TABLE IF EXISTS `v_demo1` */;

/*!50001 CREATE TABLE  `v_demo1`(
 `user_id` int(255) ,
 `tel` varchar(255) ,
 `real_name` varchar(255) 
)*/;

/*View structure for view v_demo */

/*!50001 DROP TABLE IF EXISTS `v_demo` */;
/*!50001 DROP VIEW IF EXISTS `v_demo` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_demo` AS select `u`.`user_id` AS `user_id`,`u`.`tel` AS `tel` from `user` `u` */;

/*View structure for view v_demo1 */

/*!50001 DROP TABLE IF EXISTS `v_demo1` */;
/*!50001 DROP VIEW IF EXISTS `v_demo1` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_demo1` AS select `u`.`user_id` AS `user_id`,`u`.`tel` AS `tel`,`uu`.`real_name` AS `real_name` from (`user` `u` left join `userinfo` `uu` on((`u`.`user_id` = `uu`.`user_id`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
