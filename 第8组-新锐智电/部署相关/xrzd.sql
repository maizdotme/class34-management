/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : xrzd

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-07-08 10:12:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for active
-- ----------------------------
DROP TABLE IF EXISTS `active`;
CREATE TABLE `active` (
  `active_id` int(11) NOT NULL AUTO_INCREMENT,
  `active_name` varchar(255) DEFAULT NULL,
  `active_message` varchar(255) DEFAULT NULL,
  `active_newdata` date DEFAULT NULL,
  `active_olddata` date DEFAULT NULL,
  `active_stutic` int(11) DEFAULT NULL,
  PRIMARY KEY (`active_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of active
-- ----------------------------

-- ----------------------------
-- Table structure for agent
-- ----------------------------
DROP TABLE IF EXISTS `agent`;
CREATE TABLE `agent` (
  `agent_id` int(11) NOT NULL,
  `agent_username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `agent_password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `level_parentId` int(11) DEFAULT NULL COMMENT '当前代理的父级代理id',
  `level_id` int(11) DEFAULT NULL COMMENT '123代理等级',
  `agent_tel` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '联系电话',
  `agent_addr` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '地址，代理区域',
  `agent_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '负责人姓名',
  `agent_bankNumber` int(11) DEFAULT NULL COMMENT '银行卡',
  `agent_createDate` datetime DEFAULT NULL COMMENT '成为代理时间',
  `agent_status` int(2) DEFAULT NULL COMMENT '账户状态(1/2) 代理中,代理结束',
  `agent_depositmoney` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '账户押金',
  `agent_rentmoney` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '账户余额',
  `contract_id` int(11) DEFAULT NULL COMMENT '合同id',
  `agent_img` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '头像',
  `agent_sex` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `agent_age` int(11) DEFAULT NULL,
  `agent_idcard` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of agent
-- ----------------------------

-- ----------------------------
-- Table structure for agentbettery
-- ----------------------------
DROP TABLE IF EXISTS `agentbettery`;
CREATE TABLE `agentbettery` (
  `ab_id` int(11) NOT NULL COMMENT '加盟商电池的编号',
  `battery_id` int(11) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `ab_data` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`ab_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of agentbettery
-- ----------------------------

-- ----------------------------
-- Table structure for battery
-- ----------------------------
DROP TABLE IF EXISTS `battery`;
CREATE TABLE `battery` (
  `battery_id` int(11) NOT NULL,
  `model_id` int(11) DEFAULT NULL COMMENT '电池种类Id',
  `b_date` datetime(6) DEFAULT NULL COMMENT '出场时间',
  `b_sale` int(2) DEFAULT NULL COMMENT '电池是否出厂 0:未出厂 1:出厂',
  `b_life` int(2) DEFAULT NULL COMMENT '电池状态 1:正常,2:损坏,3:丢失',
  `b_qrcode` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '电池的二维码',
  PRIMARY KEY (`battery_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of battery
-- ----------------------------

-- ----------------------------
-- Table structure for battery_distribution
-- ----------------------------
DROP TABLE IF EXISTS `battery_distribution`;
CREATE TABLE `battery_distribution` (
  `bd_id` int(11) NOT NULL AUTO_INCREMENT,
  `dr_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `battery_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`bd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of battery_distribution
-- ----------------------------

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company
-- ----------------------------

-- ----------------------------
-- Table structure for company_permission
-- ----------------------------
DROP TABLE IF EXISTS `company_permission`;
CREATE TABLE `company_permission` (
  `company_permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `mid` int(11) DEFAULT NULL,
  PRIMARY KEY (`company_permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company_permission
-- ----------------------------

-- ----------------------------
-- Table structure for consumer
-- ----------------------------
DROP TABLE IF EXISTS `consumer`;
CREATE TABLE `consumer` (
  `consumer_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '消费者编号',
  `consumer_username` varchar(255) DEFAULT NULL COMMENT '消费者账户',
  `consumer_password` varchar(255) DEFAULT NULL COMMENT '消费者密码',
  `consumer_IDcard` int(255) DEFAULT NULL COMMENT '消费者的身份证',
  `consumer_addr` varchar(255) DEFAULT NULL COMMENT '地址',
  `consumer_money` double(255,0) DEFAULT NULL COMMENT '余额',
  `consumer_sex` varchar(255) DEFAULT NULL COMMENT '性别',
  `consumer_age` int(255) DEFAULT NULL COMMENT '年龄',
  `consumer_name` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `consumer_data` datetime(6) DEFAULT NULL,
  `consumer_inviteCode` int(11) DEFAULT NULL COMMENT '邀请码',
  `consumer_tel` varchar(255) DEFAULT NULL COMMENT '电话号码',
  `consumer_img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`consumer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of consumer
-- ----------------------------

-- ----------------------------
-- Table structure for contract
-- ----------------------------
DROP TABLE IF EXISTS `contract`;
CREATE TABLE `contract` (
  `contract_id` int(255) NOT NULL AUTO_INCREMENT,
  `contract_date` datetime(6) DEFAULT NULL COMMENT '签约时间',
  `agent_a` int(11) DEFAULT NULL,
  `agent_b` int(11) DEFAULT NULL,
  `contract_filepath` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '合同文件路径',
  `contract_number` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '常量+签署日期+UUID',
  PRIMARY KEY (`contract_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of contract
-- ----------------------------

-- ----------------------------
-- Table structure for deposit
-- ----------------------------
DROP TABLE IF EXISTS `deposit`;
CREATE TABLE `deposit` (
  `deposit_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户押金id',
  `deposit_money` double(255,0) DEFAULT NULL COMMENT '用户押金',
  `deposit_startDate` datetime DEFAULT NULL COMMENT '押金收取时间',
  `deposit_endDate` datetime DEFAULT NULL COMMENT '押金退还时间',
  `deposit_status` int(255) DEFAULT NULL COMMENT '押金退还状态',
  `consumer_id` int(11) DEFAULT NULL COMMENT '消费者id',
  PRIMARY KEY (`deposit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of deposit
-- ----------------------------

-- ----------------------------
-- Table structure for distribution_record
-- ----------------------------
DROP TABLE IF EXISTS `distribution_record`;
CREATE TABLE `distribution_record` (
  `dr_id` int(11) NOT NULL AUTO_INCREMENT,
  `dr_a` int(11) DEFAULT NULL,
  `dr_b` int(11) DEFAULT NULL,
  `dr_time` datetime DEFAULT NULL,
  `battery_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`dr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of distribution_record
-- ----------------------------

-- ----------------------------
-- Table structure for empinfo
-- ----------------------------
DROP TABLE IF EXISTS `empinfo`;
CREATE TABLE `empinfo` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `empname` varchar(255) DEFAULT NULL,
  `empinfo_tel` varchar(255) DEFAULT NULL,
  `empinfo_password` varchar(255) DEFAULT NULL,
  `empinfo_sex` int(11) DEFAULT NULL,
  `empinfo_age` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`eid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of empinfo
-- ----------------------------

-- ----------------------------
-- Table structure for empinfo_role
-- ----------------------------
DROP TABLE IF EXISTS `empinfo_role`;
CREATE TABLE `empinfo_role` (
  `empinfo_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) DEFAULT NULL,
  `mid` int(11) DEFAULT NULL,
  PRIMARY KEY (`empinfo_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of empinfo_role
-- ----------------------------

-- ----------------------------
-- Table structure for emp_role
-- ----------------------------
DROP TABLE IF EXISTS `emp_role`;
CREATE TABLE `emp_role` (
  `emp_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `eid` int(11) DEFAULT NULL,
  `rid` int(11) DEFAULT NULL,
  PRIMARY KEY (`emp_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of emp_role
-- ----------------------------

-- ----------------------------
-- Table structure for invite
-- ----------------------------
DROP TABLE IF EXISTS `invite`;
CREATE TABLE `invite` (
  `invite_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '邀请表编号',
  `consumer_id` int(11) DEFAULT NULL COMMENT '邀请人id',
  `be_invited` int(255) DEFAULT NULL COMMENT '被邀请人id',
  `invite_date` datetime DEFAULT NULL COMMENT '邀请时间',
  PRIMARY KEY (`invite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of invite
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_newtel` varchar(255) DEFAULT NULL,
  `message_oidtel` varchar(255) DEFAULT NULL,
  `message_content` varchar(255) DEFAULT NULL,
  `message_headline` varchar(255) DEFAULT NULL,
  `message_data` datetime DEFAULT NULL,
  `message_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of message
-- ----------------------------

-- ----------------------------
-- Table structure for model
-- ----------------------------
DROP TABLE IF EXISTS `model`;
CREATE TABLE `model` (
  `model_id` int(11) NOT NULL COMMENT '电池型号id',
  `model_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '型号名称',
  `battery_money` decimal(10,0) DEFAULT NULL COMMENT '出厂价',
  `model_details` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '电池型号介绍',
  `model_mileage` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '电池里程',
  `model_rent` decimal(10,0) DEFAULT NULL COMMENT '电池租金',
  PRIMARY KEY (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of model
-- ----------------------------

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户订单编号',
  `comsumer_id` int(11) DEFAULT NULL COMMENT '消费者编号',
  `battery_id` int(11) DEFAULT NULL COMMENT '电池编号',
  `order_createtime` datetime DEFAULT NULL COMMENT '下单时间',
  `order_backtime` datetime DEFAULT NULL COMMENT '归还时间',
  `order_deposit` double(255,0) DEFAULT NULL COMMENT '订单押金',
  `order_rent` double(255,0) DEFAULT NULL COMMENT '订单租金',
  `order_status` int(255) DEFAULT NULL COMMENT '订单状态',
  `order_place` varchar(255) DEFAULT NULL COMMENT '订单地点',
  `order_timelong` int(11) DEFAULT NULL COMMENT '订单时长',
  `agent_id` int(11) DEFAULT NULL COMMENT '关联的网点',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for pact
-- ----------------------------
DROP TABLE IF EXISTS `pact`;
CREATE TABLE `pact` (
  `pact_id` int(11) NOT NULL COMMENT '合同编号',
  `pact_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '合同标题',
  `pact_message` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '合同内容',
  `level_id` int(11) DEFAULT NULL,
  `contract_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`pact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of pact
-- ----------------------------

-- ----------------------------
-- Table structure for qr_code
-- ----------------------------
DROP TABLE IF EXISTS `qr_code`;
CREATE TABLE `qr_code` (
  `qr_id` int(11) NOT NULL AUTO_INCREMENT,
  `qr_imgpath` varchar(255) DEFAULT NULL,
  `battery_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`qr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qr_code
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `role_permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) DEFAULT NULL,
  `mid` int(11) DEFAULT NULL,
  PRIMARY KEY (`role_permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_permission
-- ----------------------------

-- ----------------------------
-- Table structure for voncher
-- ----------------------------
DROP TABLE IF EXISTS `voncher`;
CREATE TABLE `voncher` (
  `v_id` int(11) NOT NULL AUTO_INCREMENT,
  `v_money` double(255,0) DEFAULT NULL COMMENT '抵用券面额',
  `v_gettime` datetime DEFAULT NULL COMMENT '领取时间',
  `v_overtime` datetime DEFAULT NULL COMMENT '过期时间',
  `v_name` varchar(255) DEFAULT NULL COMMENT '抵用券名字',
  `v_conditionx` varchar(255) DEFAULT NULL COMMENT '使用条件',
  `consumer_id` int(11) DEFAULT NULL COMMENT '消费者的ID',
  PRIMARY KEY (`v_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of voncher
-- ----------------------------
